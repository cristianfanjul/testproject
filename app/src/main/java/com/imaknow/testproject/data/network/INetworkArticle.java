package com.imaknow.testproject.data.network;

import com.imaknow.testproject.interactor.IRequestArticlesCallBack;

/**
 * Created by Cristian Fanjul on 19/10/2016.
 */
public interface INetworkArticle {
    /**
     * Request the list of articles to the webservice.
     *
     * @param page page number.
     * @param callback IRequestArticlesCallBack.
     */
    void requestArticlesList(int page, @CatalogWebService.SortType String sortType, IRequestArticlesCallBack callback);

    void filterArticles(int page, IRequestArticlesCallBack mRequestArticleCallBack, String mQuery);
}
