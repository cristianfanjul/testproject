package com.imaknow.testproject.data.network;

import com.imaknow.testproject.data.model.Article;
import com.imaknow.testproject.interactor.IRequestArticlesCallBack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Cristian Fanjul on 19/10/2016.
 */
public class NetworkArticle implements INetworkArticle {
    private static final int ITEMS_LIMIT_PER_PAGE = 30;
    private CatalogWebService mService;


    public NetworkArticle() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CatalogWebService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(CatalogWebService.class);
    }


    @Override
    public void requestArticlesList(final int page, @CatalogWebService.SortType final String sortType, final IRequestArticlesCallBack callback) {
        Call<List<Article>> call = mService.listArticles(sortType);
        call.enqueue(new retrofit2.Callback<List<Article>>() {
            @Override
            public void onResponse(Call<List<Article>> call, Response<List<Article>> response) {
                if (response.isSuccessful() && !response.body().isEmpty()) {
                    List<Article> articles = response.body();

                    // Simulate data sorted by the webservice.
                    sortList(articles, sortType);
                    List<Article> pagedList = getPagedList(page, articles);
                    callback.onArticlesReceived(pagedList);
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Article>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void filterArticles(final int page, final IRequestArticlesCallBack callback, final String mQuery) {
        Call<List<Article>> call = mService.filterArticles(mQuery);
        call.enqueue(new retrofit2.Callback<List<Article>>() {
            @Override
            public void onResponse(Call<List<Article>> call, Response<List<Article>> response) {
                if (response.isSuccessful() && !response.body().isEmpty()) {
                    List<Article> articles = response.body();

                    // Simulate data filtered by the webservice.
                    List<Article> filteredList = getFilteredList(articles, mQuery);
                    List<Article> pagedList = getPagedList(page, filteredList);
                    callback.onArticlesReceived(pagedList);
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Article>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    /**
     * Returns a list with the articles that contain a given name.
     *
     * @param articles List of {@link Article}.
     * @param filter   String with the word to filter.
     * @return List of {@link Article} filtered.
     */
    private List<Article> getFilteredList(List<Article> articles, String filter) {
        List<Article> filteredList;
        if (filter == null || filter.isEmpty()) {
            filteredList = articles;
        } else {
            filteredList = new ArrayList();
            for (Article article : articles) {
                if (article.getName().contains(filter)) {
                    filteredList.add(article);
                }
            }
        }

        return filteredList;
    }

    /**
     * Sorts the list alphabetically.
     *
     * @param articles List of object {@link Article}.
     * @param sortType String {@link CatalogWebService.SortType}
     */
    private void sortList(List<Article> articles, @CatalogWebService.SortType final String sortType) {
        Collections.sort(articles, new Comparator<Article>() {
            @Override
            public int compare(Article lhs, Article rhs) {
                int result;
                if (sortType.equals(CatalogWebService.SORT_A_TO_Z)) {
                    result = lhs.getName().compareTo(rhs.getName());
                } else {
                    result = rhs.getName().compareTo(lhs.getName());
                }

                return result;
            }
        });
    }

    /**
     * Returns the list paged.
     *
     * @param page     int page.
     * @param articles List of object {@link Article}.
     * @return List of articles paged.
     */
    private List<Article> getPagedList(int page, List<Article> articles) {
        List<Article> pagedList = new ArrayList<>();
        int startIndex = (page - 1) * ITEMS_LIMIT_PER_PAGE;
        int endIndex = page * (ITEMS_LIMIT_PER_PAGE - 1);
        if (articles.size() < endIndex) {
            endIndex = articles.size();
        }
        for (int i = startIndex; i < endIndex; i++) {
            pagedList.add(articles.get(i));
        }
        return pagedList;
    }


}
