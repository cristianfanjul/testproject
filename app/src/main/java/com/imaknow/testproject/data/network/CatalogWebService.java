package com.imaknow.testproject.data.network;

import android.support.annotation.StringDef;

import com.imaknow.testproject.data.model.Article;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Cristian Fanjul on 19/10/2016.
 */
public interface CatalogWebService {

    String BASE_URL = "https://api.myjson.com/bins/";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({SORT_A_TO_Z, SORT_Z_TO_A})
    public @interface SortType {
    }

    public static final String SORT_A_TO_Z = "asc";
    public static final String SORT_Z_TO_A = "desc";

    @GET("3q220")
    Call<List<Article>> listArticles(@Query("sortType") @SortType String sortType);

    @GET("3q220")
    Call<List<Article>> filterArticles(@Query("filter") String mQuery);

}
