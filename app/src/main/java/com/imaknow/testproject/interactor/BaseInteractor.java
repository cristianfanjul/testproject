package com.imaknow.testproject.interactor;

import android.support.annotation.IntDef;

import com.imaknow.testproject.data.network.INetworkArticle;
import com.imaknow.testproject.data.network.NetworkArticle;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

/**
 * Created by Cristian Fanjul on 20/10/2016.
 */
public abstract class BaseInteractor implements IInteractor {

    // Use instead of enums to optimize memory
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({WEBSERVICE_REQUEST, DATABASE_REQUEST})
    public @interface RequestType {
    }

    public static final int WEBSERVICE_REQUEST = 0;
    public static final int DATABASE_REQUEST = 1;

    protected IRequestArticlesCallBack mRequestArticleCallBack;
    @RequestType
    protected int mRequestType;
    protected int mPage;

    public BaseInteractor(IRequestArticlesCallBack requestArticlesCallBack, @RequestType int requestType, int page) {
        mRequestArticleCallBack = requestArticlesCallBack;
        mRequestType = requestType;
        mPage = page;
    }

    @Override
    public void execute() {
        if (mRequestType == WEBSERVICE_REQUEST) {
           executeOnline();
        } else {
            executeDatabase();
        }
    }

}
