package com.imaknow.testproject.interactor;

import com.imaknow.testproject.data.network.INetworkArticle;
import com.imaknow.testproject.data.network.NetworkArticle;

/**
 * Created by Cristian Fanjul on 20/10/2016.
 */
public class FilteredArticlesInteractor extends BaseInteractor implements IInteractor {
    private String mQuery;

    public FilteredArticlesInteractor(IRequestArticlesCallBack requestArticlesCallBack, String query, int page) {
        this(requestArticlesCallBack, WEBSERVICE_REQUEST, query, page);
        mQuery = query;
    }

    public FilteredArticlesInteractor(IRequestArticlesCallBack requestArticlesCallBack, @RequestType int requestType, String query, int page) {
        super(requestArticlesCallBack, requestType, page);
        mQuery = query;
    }

    @Override
    public void executeOnline() {
        INetworkArticle service = new NetworkArticle();
        service.filterArticles(mPage, mRequestArticleCallBack, mQuery);
    }

    @Override
    public void executeDatabase() {

    }
}
