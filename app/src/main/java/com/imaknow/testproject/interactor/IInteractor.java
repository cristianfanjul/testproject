package com.imaknow.testproject.interactor;

/**
 * Created by Cristian Fanjul on 18/10/2016.
 */
public interface IInteractor {
    void execute();
    void executeOnline();
    void executeDatabase();
}
