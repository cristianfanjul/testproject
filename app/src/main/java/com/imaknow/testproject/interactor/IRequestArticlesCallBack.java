package com.imaknow.testproject.interactor;

import com.imaknow.testproject.data.model.Article;

import java.util.List;

/**
 * Created by Cristian Fanjul on 18/10/2016.
 */
public interface IRequestArticlesCallBack {
    void onArticlesReceived(List<Article> articleList);

    void onError(String error);
}
