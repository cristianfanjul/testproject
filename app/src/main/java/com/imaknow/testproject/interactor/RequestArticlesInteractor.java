package com.imaknow.testproject.interactor;

import com.imaknow.testproject.data.model.Article;
import com.imaknow.testproject.data.network.CatalogWebService;
import com.imaknow.testproject.data.network.INetworkArticle;
import com.imaknow.testproject.data.network.NetworkArticle;
import com.imaknow.testproject.misc.MyAsyncTask;
import com.imaknow.testproject.misc.OnDoInBackground;
import com.imaknow.testproject.misc.OnDoInPostExecute;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristian Fanjul on 18/10/2016.
 */
public class RequestArticlesInteractor extends BaseInteractor implements IInteractor {

    @CatalogWebService.SortType
    private final String mSortType;

    public RequestArticlesInteractor(IRequestArticlesCallBack requestArticlesCallBack, int page) {
        this(requestArticlesCallBack, WEBSERVICE_REQUEST, CatalogWebService.SORT_A_TO_Z, page);
    }

    public RequestArticlesInteractor(IRequestArticlesCallBack requestArticlesCallBack, @RequestType int requestType, @CatalogWebService.SortType String sortType, int page) {
        super(requestArticlesCallBack, requestType, page);
        mSortType = sortType;
    }

    @Override
    public void executeOnline() {
        INetworkArticle service = new NetworkArticle();
        service.requestArticlesList(mPage, mSortType, mRequestArticleCallBack);
    }

    @Override
    public void executeDatabase() {
        // TODO: use database to navigate offline.
    }


    private void createFakeData(final int page) {
        final List<Article> mArticleList = new ArrayList<>();
        // TODO: call retrofit and remove this data.
        OnDoInBackground doInBackground = new OnDoInBackground() {
            @Override
            public void doInBackground() {
                for (int i = 0; i < 50; i++) {
                    mArticleList.add(new Article("Article " + (i + (50 * page)), "Description " + (i + (50 * page)), "http://www.hipercor.es/sgfm/SGFM/dctm/MEDIA03/201603/10/00197578505774____1__210x210.jpg"));
                }
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        OnDoInPostExecute onDoInPostExecute = new OnDoInPostExecute() {
            @Override
            public void doInPostExecute() {
                mRequestArticleCallBack.onArticlesReceived(mArticleList);
            }
        };
        MyAsyncTask asyncTask = new MyAsyncTask(doInBackground, onDoInPostExecute);
        asyncTask.execute();
    }
}
