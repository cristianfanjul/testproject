package com.imaknow.testproject.misc;

/**
 * Created by Cristian Fanjul on 18/10/2016.
 */
public interface OnDoInPostExecute{
    void doInPostExecute();
}
