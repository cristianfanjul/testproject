package com.imaknow.testproject.misc;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Cristian Fanjul on 25/07/2015.
 */
public class Utils {
    public static int getScreenWidth(Context context) {
        int columnWidth;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            if (Build.VERSION.SDK_INT >= 13) {
                display.getSize(point);
            } else {
                point.x = display.getWidth();
                point.y = display.getHeight();
            }
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }

        columnWidth = point.x;
        return columnWidth;
    }
}
