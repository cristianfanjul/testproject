package com.imaknow.testproject.misc;

import android.app.Application;
import android.content.Context;

/**
 * Created by Cristian Fanjul on 17/10/2016.
 */

public class TestApplication extends Application {
    public static final String PAGER_POSITION_KEY = "pager_position";
    public static final String ARTICLES_LIST_KEY = "articles_list";
    private static TestApplication sContext;

    public static Context getInstance() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
    }
}
