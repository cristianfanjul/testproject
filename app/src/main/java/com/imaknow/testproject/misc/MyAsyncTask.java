package com.imaknow.testproject.misc;

import android.os.AsyncTask;

/**
 * Created by Cristian Fanjul on 09/06/2016.
 */
public class MyAsyncTask extends AsyncTask<Void, Void, Void> {
    private OnDoInBackground mOnDoInBackground;
    private OnDoInPostExecute mOnDoInPostExecute;

    public MyAsyncTask(OnDoInBackground onDoInBackground) {
        mOnDoInBackground = onDoInBackground;
    }

    public MyAsyncTask(OnDoInBackground onDoInBackground, OnDoInPostExecute onAsyncExecution) {
        mOnDoInBackground = onDoInBackground;
        mOnDoInPostExecute = onAsyncExecution;
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (mOnDoInBackground != null) {
            mOnDoInBackground.doInBackground();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (mOnDoInPostExecute != null) {
            mOnDoInPostExecute.doInPostExecute();
        }
    }
}