package com.imaknow.testproject.ui.main;

import com.imaknow.testproject.data.network.CatalogWebService;
import com.imaknow.testproject.interactor.BaseInteractor;

/**
 * Created by Cristian Fanjul on 18/10/2016.
 */
public interface IMainPresenter extends IBasePresenter {
    void requestArticles(int page);

    void requestArticles(int page, @BaseInteractor.RequestType int requestType, @CatalogWebService.SortType String sortType);
}
