package com.imaknow.testproject.ui.fullscreen;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.imaknow.testproject.R;
import com.imaknow.testproject.data.model.Article;

import java.util.List;


public class FullScreenArticleAdapter extends PagerAdapter {

    private Activity mActivity;
    private List<Article> mArticlesList;
    private LayoutInflater inflater;

    // constructor
    public FullScreenArticleAdapter(Activity activity, List<Article> myImageList) {
        mActivity = activity;
        mArticlesList = myImageList;
    }

    @Override
    public int getCount() {
        return mArticlesList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ScrollView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_article, container, false);

        ImageView imgVwArticle = (ImageView) viewLayout.findViewById(R.id.img_vw_fullscreen_article);
        TextView txtVwTitle = (TextView) viewLayout.findViewById(R.id.txt_vw_fullscreen_title);
        TextView txtVwDesctiption = (TextView) viewLayout.findViewById(R.id.txt_vw_fullscreen_description);

        Article article = mArticlesList.get(position);

        if (article.getImage() != null && !article.getImage().isEmpty()) {
            loadImage(article, imgVwArticle);
        } else {
            imgVwArticle.setImageDrawable(mActivity.getDrawable(R.drawable.default_little));
        }
        txtVwTitle.setText(article.getName());
        txtVwDesctiption.setText(article.getDescription());

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    private void loadImage(Article article, ImageView imageView) {
        Glide.with(mActivity)
                .load(article.getImage())
                .centerCrop()
                .crossFade()
                .into(imageView);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ScrollView) object);
    }

    public void clear() {
        mArticlesList.clear();
    }


    public void addAll(List<Article> articleList) {
        mArticlesList.addAll(articleList);
    }
}
