package com.imaknow.testproject.ui.fullscreen;

import com.imaknow.testproject.ui.main.IBasePresenter;

/**
 * Created by Cristian Fanjul on 17/10/2016.
 */
public interface IFullScreenPresenter extends IBasePresenter {
}
