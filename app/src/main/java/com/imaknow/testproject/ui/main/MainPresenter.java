package com.imaknow.testproject.ui.main;

import com.imaknow.testproject.data.network.CatalogWebService;
import com.imaknow.testproject.interactor.BaseInteractor;
import com.imaknow.testproject.interactor.IInteractor;
import com.imaknow.testproject.interactor.RequestArticlesInteractor;

/**
 * Created by Cristian Fanjul on 18/10/2016.
 */
public class MainPresenter extends BasePresenter implements IMainPresenter {

    public MainPresenter(IMainView mainView) {
        super(mainView);
    }

    @Override
    public void requestArticles(int page) {
        IInteractor requestArticlesInteractor = new RequestArticlesInteractor(this, page);
        requestArticlesInteractor.execute();
    }

    @Override
    public void requestArticles(int page, @BaseInteractor.RequestType int requestType, @CatalogWebService.SortType String sortType) {
        IInteractor requestArticlesInteractor = new RequestArticlesInteractor(this, requestType, sortType, page);
        requestArticlesInteractor.execute();
    }
}
