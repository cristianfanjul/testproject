package com.imaknow.testproject.ui.filter;

/**
 * Created by Cristian Fanjul on 20/10/2016.
 */
public interface IFilterPresenter {
    /**
     * Filters the articles using a key word.
     *
     * @param page  page to bring.
     * @param query Word to filter.
     */
    void requestFilterArticles(int page, String query);
}
