package com.imaknow.testproject.ui.fullscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.imaknow.testproject.R;
import com.imaknow.testproject.data.model.Article;
import com.imaknow.testproject.misc.TestApplication;

import java.util.ArrayList;
import java.util.List;


public class FullScreenViewActivity extends AppCompatActivity implements IFullScreenView {

    private FullScreenArticleAdapter mFullScreenArticleAdapter;
    private ViewPager mViewPager;
    private ProgressBar mProgressBar;
    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);
        mFullScreenArticleAdapter = new FullScreenArticleAdapter(FullScreenViewActivity.this,
                new ArrayList<Article>());

        mProgressBar = (ProgressBar) findViewById(R.id.prog_bar_fullscreen);
        mViewPager = (ViewPager) findViewById(R.id.pager_fullscreen);
        mViewPager.setAdapter(mFullScreenArticleAdapter);

        Intent i = getIntent();
        mPosition = i.getIntExtra(TestApplication.PAGER_POSITION_KEY, 0);
        List<Article> articles = getIntent().getParcelableArrayListExtra(TestApplication.ARTICLES_LIST_KEY);
        if (articles != null) {
            updatePager(articles);
        }
    }

    private void updatePager(List<Article> articleList) {
        mProgressBar.setVisibility(View.GONE);
        mFullScreenArticleAdapter.clear();
        mFullScreenArticleAdapter.addAll(articleList);
        mFullScreenArticleAdapter.notifyDataSetChanged();

        // Displaying selected article first.
        mViewPager.setCurrentItem(mPosition);
        mViewPager.setVisibility(View.VISIBLE);
    }

}
