package com.imaknow.testproject.ui.main;

import android.util.Log;

import com.imaknow.testproject.data.model.Article;
import com.imaknow.testproject.interactor.IRequestArticlesCallBack;

import java.util.List;

/**
 * Created by Cristian Fanjul on 20/10/2016.
 */
public class BasePresenter implements IBasePresenter, IRequestArticlesCallBack {
    private IBaseView mBaseView;

    public BasePresenter(IBaseView baseView) {
        mBaseView = baseView;
    }

    @Override
    public void onArticlesReceived(List<Article> articleList) {
        mBaseView.onArticleListReceived(articleList);
    }

    @Override
    public void onError(String error) {
        Log.e("MainPresenter", error);
    }
}
