package com.imaknow.testproject.ui.fullscreen;

import android.util.Log;

import com.imaknow.testproject.data.model.Article;
import com.imaknow.testproject.interactor.IInteractor;
import com.imaknow.testproject.interactor.IRequestArticlesCallBack;
import com.imaknow.testproject.interactor.RequestArticlesInteractor;

import java.util.List;

/**
 * Created by Cristian Fanjul on 17/10/2016.
 */
public class FullScreenPresenter implements IFullScreenPresenter, IRequestArticlesCallBack {

    private IFullScreenView mFullScreenView;

    public FullScreenPresenter(IFullScreenView fullScreenView) {
        mFullScreenView = fullScreenView;
    }

    @Override
    public void onArticlesReceived(List<Article> articleList) {
//        mFullScreenView.onArticleListReceived(articleList);
    }

    @Override
    public void onError(String error) {
        Log.e("FullScreenPresenter", error);
    }
}
