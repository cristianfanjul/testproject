package com.imaknow.testproject.ui.filter;

import com.imaknow.testproject.interactor.FilteredArticlesInteractor;
import com.imaknow.testproject.interactor.IInteractor;
import com.imaknow.testproject.ui.main.BasePresenter;

/**
 * Created by Cristian Fanjul on 20/10/2016.
 */
public class FilterPresenter extends BasePresenter implements IFilterPresenter {

    public FilterPresenter(IFilterView filterView) {
        super(filterView);
    }

    @Override
    public void requestFilterArticles(int page, String query) {
        IInteractor filteredArticlesInteractor = new FilteredArticlesInteractor(this, query, page);
        filteredArticlesInteractor.execute();
    }
}
