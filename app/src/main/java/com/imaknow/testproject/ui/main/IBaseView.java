package com.imaknow.testproject.ui.main;

import com.imaknow.testproject.data.model.Article;

import java.util.List;

/**
 * Created by Cristian Fanjul on 18/10/2016.
 */
public interface IBaseView {
    /**
     * Request more articles in case there are many pages to show.
     */
    void requestMoreArticles();

    /**
     * Receives a list of object type {@link Article}.
     *
     * @param articleList List of {@link Article}.
     */
    void onArticleListReceived(List<Article> articleList);
}
