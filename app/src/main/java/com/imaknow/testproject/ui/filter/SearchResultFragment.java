package com.imaknow.testproject.ui.filter;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;

import com.imaknow.testproject.R;
import com.imaknow.testproject.ui.main.GridFragment;

/**
 * Created by Cristian Fanjul on 20/10/2016.
 */
public class SearchResultFragment extends GridFragment implements IFilterView {

    private IFilterPresenter mFilterPresenter;
    private String mQuery;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mFilterPresenter = new FilterPresenter(this);

        handleIntent(getActivity().getIntent());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);

        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setIconified(false);
        searchView.setQuery(mQuery, false);
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                showProgress();
                mAdapter.clear();
                mPage = 1;
                mFilterPresenter.requestFilterArticles(mPage, query);
                mQuery = query;

                // Avoid calling the listener twice. Android bug: https://code.google.com/p/android/issues/detail?id=24599
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            mFilterPresenter.requestFilterArticles(mPage, query);
            mQuery = query;
        }
    }

    @Override
    public void requestMoreArticles() {
        mFilterPresenter.requestFilterArticles(++mPage, mQuery);
    }
}
