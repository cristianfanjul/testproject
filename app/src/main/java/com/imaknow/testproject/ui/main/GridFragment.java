package com.imaknow.testproject.ui.main;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.imaknow.testproject.R;
import com.imaknow.testproject.data.model.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristian Fanjul on 20/10/2016.
 */

public abstract class GridFragment extends Fragment implements IBaseView {

    protected View mRootView;
    protected List<Article> mArticleList;
    protected RecyclerViewArticleAdapter mAdapter;
    protected RecyclerView mRecyclerView;
    protected int mPage;
    protected GridLayoutManager mLayoutManager;
    protected IBasePresenter mBasePresenter;
    protected ProgressBar mProgressBarGrid;
    protected FloatingActionsMenu mFloatingMenu;
    protected FloatingActionButton mFloatingAtoZ;
    protected FloatingActionButton mFloatingZtoA;

    // The minimum amount of items to have below your current scroll position before loading more.
    protected int mVisibleThreshold = 1;
    protected int mLastVisibleItem, mTotalItemCount;
    protected boolean mLoading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = 1;
        mBasePresenter = new BasePresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_grid, container, false);
        mProgressBarGrid = (ProgressBar) mRootView.findViewById(R.id.prog_bar_grid);
        initFloatingButtons();

        initGridView();

        mAdapter.clear();
        updateGridContent();

        return mRootView;
    }

    private void initFloatingButtons() {
        mFloatingMenu = (FloatingActionsMenu) mRootView.findViewById(R.id.btn_multiple_actions);
        mFloatingAtoZ = (FloatingActionButton) mRootView.findViewById(R.id.btn_order_a_to_z);
        mFloatingZtoA = (FloatingActionButton) mRootView.findViewById(R.id.btn_order_z_to_a);
    }

    protected void updateGridContent() {
        if (mArticleList != null) {
            mAdapter.addAll(mArticleList);
            mAdapter.notifyDataSetChanged();
        }
    }

    protected void initGridView() {
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_vw_articles);
        mRecyclerView.addItemDecoration(new MarginDecoration(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mAdapter = new RecyclerViewArticleAdapter(getActivity(), new ArrayList());
        mRecyclerView.setAdapter(mAdapter);

        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mAdapter.getItemViewType(position)) {
                    case RecyclerViewArticleAdapter.VIEW_ITEM:
                        return 1;
                    case RecyclerViewArticleAdapter.VIEW_PROG:
                        return 3; //number of columns of the grid
                    default:
                        return -1;
                }
            }
        });
        mRecyclerView.setLayoutManager(mLayoutManager);
        addScrollPager();
    }

    private void addScrollPager() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    mTotalItemCount = mLayoutManager.getItemCount();
                    mLastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                    if (!mLoading && mTotalItemCount <= (mLastVisibleItem + mVisibleThreshold)) {
                        // End has been reached
                        mAdapter.add(null); // Progress View
                        mAdapter.notifyDataSetChanged();
                        requestMoreArticles();
                        mLoading = true;
                    }
                }
            }
        });
    }

    public boolean isLoading() {
        return mLoading;
    }

    private class MarginDecoration extends RecyclerView.ItemDecoration {
        private int mMargin, mMarginTop;

        public MarginDecoration(Context context) {
            mMargin = context.getResources().getDimensionPixelSize(R.dimen.item_margin_sides);
        }

        @Override
        public void getItemOffsets(
                Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(mMargin, mMargin, mMargin, mMargin);
        }
    }

    @Override
    public void onArticleListReceived(List<Article> articleList) {
        mArticleList = articleList;
        if (isLoading() && mAdapter.getItemCount() > 0) {
            mAdapter.removeLastItem();
            mAdapter.notifyDataSetChanged();
            mLoading = false;
        }
        updateGridContent();
        showContent();
    }

    private void showContent() {
        mProgressBarGrid.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    protected void showProgress() {
        mProgressBarGrid.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }
}
