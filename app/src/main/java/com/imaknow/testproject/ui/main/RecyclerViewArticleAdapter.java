package com.imaknow.testproject.ui.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.imaknow.testproject.R;
import com.imaknow.testproject.data.model.Article;
import com.imaknow.testproject.misc.TestApplication;
import com.imaknow.testproject.ui.fullscreen.FullScreenViewActivity;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int VIEW_ITEM = 1;
    public static final int VIEW_PROG = 0;

    private Context mContext;
    private List<Article> mArticlesList = new ArrayList();

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    public RecyclerViewArticleAdapter(Context context, List<Article> articlesList) {
        mContext = context;
        mArticlesList = articlesList;
    }

    @Override
    public int getItemViewType(int position) {
        return mArticlesList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_article_item, null);

            vh = new CustomViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        Article article = mArticlesList.get(position);

        if (viewHolder instanceof CustomViewHolder) {
            CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;

            if (article.getImage() != null && !article.getImage().isEmpty()) {
                loadImage(customViewHolder.mImgVwArticle, article);
            } else {
                customViewHolder.mImgVwArticle.setImageDrawable(mContext.getDrawable(R.drawable.default_little));
            }

            customViewHolder.mImgVwArticle.setOnClickListener(new OnImageClickListener(position));
            customViewHolder.mTxtViewLabel.setText(article.getName());
        } else {
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }
    }

    private void loadImage(ImageView imageView, Article article) {
        Glide.with(mContext)
                .load(article.getImage())
                .centerCrop()
                .crossFade()
                .into(imageView);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return (null != mArticlesList ? mArticlesList.size() : 0);
    }

    public synchronized void addAll(List<Article> articlesList) {
        mArticlesList.addAll(articlesList);
    }

    public synchronized void clear() {
        mArticlesList.clear();
    }

    public synchronized void add(Article article) {
        mArticlesList.add(article);
    }

    public synchronized void removeLastItem() {
        mArticlesList.remove(mArticlesList.size() - 1);
    }

    class OnImageClickListener implements OnClickListener {
        int postion;

        public OnImageClickListener(int position) {
            this.postion = position;
        }

        @Override
        public void onClick(View v) {
            Intent i = new Intent(mContext, FullScreenViewActivity.class);
            i.putExtra(TestApplication.PAGER_POSITION_KEY, postion);
            i.putParcelableArrayListExtra(TestApplication.ARTICLES_LIST_KEY, (ArrayList) mArticlesList);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mContext.startActivity(i);
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView mImgVwArticle;
        protected TextView mTxtViewLabel;

        public CustomViewHolder(View view) {
            super(view);
            mImgVwArticle = (ImageView) view.findViewById(R.id.img_vw_grid_article);
            mTxtViewLabel = (TextView) view.findViewById(R.id.txt_vw_grid_label);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}

