package com.imaknow.testproject.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.imaknow.testproject.data.network.CatalogWebService;
import com.imaknow.testproject.interactor.BaseInteractor;

/**
 * Main Fragment for Activity.
 */
public class MainFragment extends GridFragment implements IMainView {

    private IMainPresenter mMainPresenter;
    @CatalogWebService.SortType
    private String mSortType = CatalogWebService.SORT_A_TO_Z;

    public static MainFragment newInstance() {
        // TODO: add bundle if needed.
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainPresenter = new MainPresenter(this);
        mMainPresenter.requestArticles(mPage);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFloatingMenu.setVisibility(View.VISIBLE);
        mFloatingAtoZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSorting(CatalogWebService.SORT_A_TO_Z);
            }
        });
        mFloatingZtoA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSorting(CatalogWebService.SORT_Z_TO_A);
            }
        });
    }

    private void performSorting(@CatalogWebService.SortType String sortType) {
        mAdapter.clear();
        mPage = 1;
        mSortType = sortType;
        mMainPresenter.requestArticles(mPage, BaseInteractor.WEBSERVICE_REQUEST, mSortType);
        mFloatingMenu.collapse();
        showProgress();
    }

    @Override
    public void requestMoreArticles() {
        mMainPresenter.requestArticles(++mPage, BaseInteractor.WEBSERVICE_REQUEST, mSortType);
    }

}
