package com.imaknow.testproject.ui.main;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.imaknow.testproject.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest2 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest2() {
        ViewInteraction viewInteraction = onView(
                allOf(withId(R.id.fab_expand_menu_button),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        viewInteraction.perform(click());

        ViewInteraction viewInteraction2 = onView(
                allOf(withId(R.id.fab_expand_menu_button),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        viewInteraction2.perform(click());

        ViewInteraction viewInteraction3 = onView(
                allOf(withId(R.id.fab_expand_menu_button),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        viewInteraction3.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.btn_order_z_to_a),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction viewInteraction4 = onView(
                allOf(withId(R.id.fab_expand_menu_button),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        viewInteraction4.perform(click());

        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.btn_order_a_to_z),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.search), withContentDescription("search"), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction searchAutoComplete = onView(
                allOf(withId(R.id.search_src_text),
                        withParent(allOf(withId(R.id.search_plate),
                                withParent(withId(R.id.search_edit_frame)))),
                        isDisplayed()));
        searchAutoComplete.perform(replaceText("77"), closeSoftKeyboard());

        pressBack();

        pressBack();

        pressBack();

        ViewInteraction viewInteraction5 = onView(
                allOf(withId(R.id.fab_expand_menu_button),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        viewInteraction5.perform(click());

        ViewInteraction floatingActionButton3 = onView(
                allOf(withId(R.id.btn_order_z_to_a),
                        withParent(withId(R.id.btn_multiple_actions)),
                        isDisplayed()));
        floatingActionButton3.perform(click());

    }

}
